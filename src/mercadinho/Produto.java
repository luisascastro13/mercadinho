/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercadinho;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luisa
 */
public abstract class Produto {
    private String nome, descricao, especial;
    private double preco;
    private int quantidade, codprod;
    
    public abstract String getTipo();
    public double getValorTotal(){
        return preco*quantidade;
    }
    
    public abstract Produto copiar();

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getEspecial() {
        return especial;
    }

    public void setEspecial(String especial) {
        this.especial = especial;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public int getCodprod() {
        return codprod;
    }

    public void setCodprod(int codprod) {
        this.codprod = codprod;
    }
    
    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    public void insert(){
        String insert = "insert into mercadoproduto(codprod, preco, descricao, nome, especial, tipo, atualmente_vendido)"
                + "values(mercadocodprod.nextval, ?, ?, ?, ?, ?, 'Sim')";
        
        String insert2 = "insert into mercadoitemestoque(codigoitemestoque, codprod, quantprod)"
                + " values(? ,? , 0)";
        
        Conexao conexao = new Conexao("dbds3_06", "123456");
        Connection conn = conexao.getConexao();
        String genColumns[] = {"codprod"};
        try{
            PreparedStatement ps = conn.prepareStatement(insert,genColumns);
            PreparedStatement ps2 = conn.prepareStatement(insert2);
            
            ps.setDouble(1, preco);
            ps.setString(2, descricao);
            ps.setString(3, nome);
            ps.setString(4, especial);
            ps.setString(5, getTipo());
          
            ps.execute();
            ResultSet idResult = ps.getGeneratedKeys();
            int id = -1;
            if(idResult.next()){
                id = idResult.getInt(1);
                System.out.println("Teve id " + id);
                this.codprod = id;
            } else {
                System.out.println("Não teve id");
            }
            
            ps2.setInt(1, id);
            ps2.setInt(2, id);
            
            ps2.execute();
            
        }
        catch(SQLException e){
           System.out.println(e);
       }      
    }
    
    public static List<Produto> getAtualmenteVendidos(){
        Conexao conexao = new Conexao("dbds3_06", "123456");
        String select = "select mercadoproduto.*, mercadoitemestoque.quantprod\n" +
                        "from mercadoproduto\n" +
                        "left join mercadoitemestoque on mercadoitemestoque.codprod = mercadoproduto.codprod\n" +
                        "where atualmente_vendido = 'Sim'";
        List<Produto> lista = new ArrayList<>();
        try{
            Connection conn = conexao.getConexao();
            PreparedStatement ps = conn.prepareStatement(select);
            
            ResultSet res = ps.executeQuery();  
            
            while(res.next()){
                Produto p;
                String tipo = res.getString("tipo");
                if(tipo.equals("AlimentoNaoPerecivel")){
                    p = new AlimentoNaoPerecivel();
                }
                else if(tipo.equals("AlimentoPerecivel")){
                    p = new AlimentoPerecivel();
                }
                else if(tipo.equals("HigienePessoal")){
                    p = new HigienePessoal();
                }
                else{ //tipo == roupa
                    p = new Roupa();
                }
                
                p.setNome(res.getString("nome"));
                p.setDescricao(res.getString("descricao"));
                p.setPreco(res.getDouble("preco"));
                p.setEspecial(res.getString("especial"));
                p.setQuantidade(res.getInt("quantprod"));
                p.setCodprod(res.getInt("codprod"));
                
                lista.add(p);
            }
            
        }
        catch(SQLException e)
        {
            System.out.println(e);
        }
        
        return lista;
    }
    
    public static Produto getProdutoPorCodProd(int id){
        String sql = "select * from mercadoproduto where codprod = ?";
        Produto p = null;
        try{ 
           Connection c = new Conexao("DBDS3_06", "123456").getConexao();
           PreparedStatement statement = c.prepareStatement(sql);
           statement.setInt(1,id);
           ResultSet res = statement.executeQuery();
           if(res.next()){
                String tipo = res.getString("tipo");
                if(tipo.equals("AlimentoNaoPerecivel")){
                    p = new AlimentoNaoPerecivel();
                }
                else if(tipo.equals("AlimentoPerecivel")){
                    p = new AlimentoPerecivel();
                }
                else if(tipo.equals("HigienePessoal")){
                    p = new HigienePessoal();
                }
                else{ //tipo == roupa
                    p = new Roupa();
                }
                
                p.setNome(res.getString("nome"));
                p.setDescricao(res.getString("descricao"));
                p.setPreco(res.getDouble("preco"));
                p.setEspecial(res.getString("especial"));
                p.setQuantidade(res.getInt("quantprod"));
                p.setCodprod(res.getInt("codprod"));
           }
        } catch(SQLException e) {
            System.out.println("Exception em getProdutoPorCodProd");
            System.out.println(e);
        }
        return p;
    }
    
    public void atualizarEstoque(){
        Conexao conexao = new Conexao("dbds3_06", "123456");
        String update = "update mercadoitemestoque" +
                        " set quantprod = ?" +
                        " where codprod = ?";
        try{
            Connection conn = conexao.getConexao();
            PreparedStatement ps = conn.prepareStatement(update);
            
            ps.setInt(1, quantidade);
            ps.setInt(2, codprod);
            
            ps.execute();
        }
        catch(SQLException e)
        {
            System.out.println(e);
        }
    }
    public void diminuirEstoque(){
        Conexao conexao = new Conexao("dbds3_06", "123456");
        String update = "update mercadoitemestoque" +
                        " set quantprod = quantprod - ?" +
                        " where codprod = ?";
        try{
            Connection conn = conexao.getConexao();
            PreparedStatement ps = conn.prepareStatement(update);
            
            ps.setInt(1, quantidade);
            ps.setInt(2, codprod);
            
            ps.execute();
        }
        catch(SQLException e)
        {
            System.out.println(e);
        }
    }
    
    
    
}
