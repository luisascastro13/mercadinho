/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercadinho;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;

/**
 * FXML Controller class
 *
 * @author luisa
 */
public class CadastrarProdutosController implements Initializable {

    @FXML
    private TextField nome;
    @FXML
    private TextField preco;
    @FXML
    private TextArea descricao;
    @FXML
    private ToggleButton alimentoPerecivel;
    @FXML
    private ToggleButton alimentoNaoPerecivel;
    @FXML
    private ToggleButton higienePessoal;
    @FXML
    private ToggleButton roupa;
    @FXML
    private TextField detalhe;
    @FXML
    private ToggleGroup tipo;
    @FXML
    private Label informe;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void cadastrar(ActionEvent event) {

        Toggle t = tipo.getSelectedToggle();
        Produto p;
        
        RadioButton radiobutton = (RadioButton) t;
        System.out.println(radiobutton.getText());
        
        if(radiobutton.getText().equals("Alimento Não Perecível")){
            p = new AlimentoNaoPerecivel();  
        }
        else if(radiobutton.getText().equals("Alimento Perecível")){
            p = new AlimentoPerecivel();
        }
        else if(radiobutton.getText().equals("Higiene Pessoal")){
            p = new HigienePessoal();
        }
        else{ //radiobutton.getText().equals("Roupas");
            p = new Roupa();
        }
        
        p.setNome(nome.getText());
        p.setPreco(Double.parseDouble(preco.getText()));
        p.setDescricao(descricao.getText());
        p.setEspecial(detalhe.getText());
        
        p.insert();
        
        nome.setText("");
        preco.setText("");
        descricao.setText("");
        detalhe.setText("");
        informe.setText("Cadastrado com sucesso.");
    }
       
    
    @FXML
    private void voltar(ActionEvent event){
        Mercadinho.trocaTela("Inicial.fxml");
    }
    
}
