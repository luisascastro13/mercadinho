/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercadinho;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author luisa
 */
public class Mercadinho extends Application {
    
    static Stage stage = null;
    
    private static Venda vendaEspecifica;

    public static Venda getVendaEspecifica() {
        return vendaEspecifica;
    }

    public static void setVendaEspecifica(Venda vendaEspecifica) {
        Mercadinho.vendaEspecifica = vendaEspecifica;
    }
    
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Inicial.fxml"));
        
        Scene scene = new Scene(root);
        Mercadinho.stage = stage;
        stage.setScene(scene);
        stage.show();
    }
    
    public static void trocaTela(String tela){
        Parent root = null;
        try{
            root = FXMLLoader.load(Mercadinho.class.getResource(tela));
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML" + e);
        }
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
