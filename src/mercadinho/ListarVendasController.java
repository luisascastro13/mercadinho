/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercadinho;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author luisa
 */
public class ListarVendasController implements Initializable {

    @FXML
    private TableColumn<?, ?> codigonf;
    @FXML
    private TableColumn<?, ?> dataTabela;
    @FXML
    private TableColumn<?, ?> valor;
    @FXML
    private TableView<Venda> tabela;
    @FXML
    private TableColumn<?, ?> nome;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tabela.setItems(FXCollections.observableArrayList(Venda.mostrarTodasVendas())); 
        
        codigonf.setCellValueFactory(new PropertyValueFactory("codigonf"));
        dataTabela.setCellValueFactory(new PropertyValueFactory("dataString"));
        valor.setCellValueFactory(new PropertyValueFactory("valor"));
        nome.setCellValueFactory(new PropertyValueFactory("nomeCliente"));
    }    

    @FXML
    private void verMais(ActionEvent event) {
        Mercadinho.setVendaEspecifica(tabela.getSelectionModel().getSelectedItem());
 
        Mercadinho.trocaTela("VendaEspecifica.fxml");
    }
    
    @FXML
    private void voltar(ActionEvent event){
        Mercadinho.trocaTela("Inicial.fxml");
    }
    
}
