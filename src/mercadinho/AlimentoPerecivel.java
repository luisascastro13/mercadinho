/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercadinho;

/**
 *
 * @author luisa
 */
public class AlimentoPerecivel extends Produto {
    
    @Override
    public String getTipo(){
        return "AlimentoPerecivel";
    }
    @Override
    public Produto copiar(){
        Produto p = new AlimentoPerecivel();
        p.setNome(this.getNome());
        p.setCodprod(this.getCodprod());
        p.setDescricao(this.getDescricao());
        p.setQuantidade(this.getQuantidade());
        p.setPreco(this.getPreco());
        p.setEspecial(this.getEspecial());
        
        return p;
    }
}
