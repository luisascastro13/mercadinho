/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercadinho;

/**
 *
 * @author luisa
 */
public class Data {
    private int dia, mes, ano;
    
    Data(int dia, int mes, int ano){
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }   
    
    Data(String dataText) throws DataException{
        String[] data = dataText.split("/");
        dia = Integer.parseInt(data[0]);
        mes = Integer.parseInt(data[1]);
        ano = Integer.parseInt(data[2]);
        
        if(dia<=0 || dia>31){
            throw new DataException("Dia inválido.");            
        }
        if(mes<1 || mes>12){
            throw new DataException("Mês inválido.");
        }
        if(dia>29 && mes==2){
            throw new DataException("Fevereiro não tem tantos dias.");
        }
    }
    
}
