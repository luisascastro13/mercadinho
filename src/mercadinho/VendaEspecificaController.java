/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercadinho;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author luisa
 */
public class VendaEspecificaController implements Initializable {

    @FXML
    private Label notaFiscal;
    @FXML
    private TableView<Produto> tabelaProdutos;
    @FXML
    private TableColumn<?, ?> produto;
    @FXML
    private TableColumn<?, ?> quant;
    @FXML
    private TableColumn<?, ?> preco;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tabelaProdutos.setItems(FXCollections.observableArrayList(
                Venda.carregarProdutosDaVenda(
                        Mercadinho.getVendaEspecifica().getCodigonf())));
        produto.setCellValueFactory(new PropertyValueFactory("nome"));
        quant.setCellValueFactory(new PropertyValueFactory("quantidade"));
        preco.setCellValueFactory(new PropertyValueFactory("preco"));
        
        notaFiscal.setText("Nota Fiscal: " + Mercadinho.getVendaEspecifica().getCodigonf());
    }    

    @FXML
    private void voltar(ActionEvent event) {
        Mercadinho.trocaTela("ListarVendas.fxml");
    }
    
}
