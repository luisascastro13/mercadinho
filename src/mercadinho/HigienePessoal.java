/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercadinho;

/**
 *
 * @author luisa
 */
public class HigienePessoal extends Produto{
    
    @Override
    public String getTipo(){
        return "HigienePessoal";
    }
    @Override
    public Produto copiar(){
        Produto p = new HigienePessoal();
        p.setNome(this.getNome());
        p.setCodprod(this.getCodprod());
        p.setDescricao(this.getDescricao());
        p.setQuantidade(this.getQuantidade());
        p.setPreco(this.getPreco());
        p.setEspecial(this.getEspecial());
        
        return p;
    }
    
}
