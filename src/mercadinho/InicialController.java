/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercadinho;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author luisa
 */
public class InicialController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void estoque(ActionEvent event) {
        Mercadinho.trocaTela("AlterarEstoque.fxml");
    }

    @FXML
    private void vender(ActionEvent event) {
        Mercadinho.trocaTela("Vender.fxml");
    }

    @FXML
    private void listarVendas(ActionEvent event) {
        Mercadinho.trocaTela("ListarVendas.fxml");
    }

    @FXML
    private void cadastrar(ActionEvent event) {
        Mercadinho.trocaTela("CadastrarProdutos.fxml");
    }
    
}
