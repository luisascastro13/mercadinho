/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercadinho;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Date;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luisa
 */
public class Venda {
    private List<Produto> produtosVenda;
    private int codigonf;
    private double valor;
    private String nomeCliente;
    private Date dataVenda;

    public Date getDataVenda(){
        return dataVenda;
    }

    public void setDataVenda(Date dataVenda) {
        this.dataVenda = dataVenda;
    }

    public String getDataString(){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return df.format(dataVenda);
    }

    public List<Produto> getProdutosVenda() {
        return produtosVenda;
    }

    public void setProdutosVenda(List<Produto> produtosVenda) {
        this.produtosVenda = produtosVenda;
    }

    public int getCodigonf() {
        return codigonf;
    }

    public void setCodigonf(int codigonf) {
        this.codigonf = codigonf;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public void insertVenda(){
        Conexao conexao = new Conexao("dbds3_06", "123456");
        Connection conn = conexao.getConexao();

        String insert = "insert into mercadovenda(codigonf, valor, nome_cliente, data)"
                + " values(codigonf.nextval, ?, ?, ?)";
        String genColumns[] = {"codigonf"};

        try{
            PreparedStatement ps = conn.prepareStatement(insert, genColumns);
            ps.setDouble(1, this.getValor());
            ps.setString(2, this.getNomeCliente());

            ps.setDate(3, dataVenda);

            ps.execute();
            ResultSet codnf = ps.getGeneratedKeys();

            int id = -1;
            if(codnf.next()){
                id = codnf.getInt(1);
                System.out.println("Teve id " + id);
                codigonf = id;
            } else {
                System.out.println("Não teve id");
            }

        }
        catch(SQLException e)
        {
            System.out.println(e);
        }
    }

    public void insertItemVenda(){
        Conexao conexao = new Conexao("dbds3_06", "123456");
        Connection conn = conexao.getConexao();

        String insertItemVenda = "insert into mercadoitemvenda(codigoitemvenda, codprod, codigonf, quantprod)"
                               + " values(codigoitemvenda.nextval, ? , ?, ?)";

        for(int i=0; i<produtosVenda.size(); i++)
        {
            try{
                PreparedStatement ps = conn.prepareStatement(insertItemVenda);
                ps.setInt(1, this.getProdutosVenda().get(i).getCodprod());
                ps.setInt(2, codigonf);
                ps.setInt(3, this.getProdutosVenda().get(i).getQuantidade());

                ps.execute();
            }
            catch(SQLException e){
                System.out.println(e);
            }
        }


    }

    public static List<Venda> mostrarTodasVendas(){
        Conexao conexao = new Conexao("dbds3_06", "123456");
        String select = "select * from mercadovenda";
        List lista = new ArrayList<>();
        try{
            Connection conn = conexao.getConexao();
            PreparedStatement ps = conn.prepareStatement(select);
            ResultSet res = ps.executeQuery();

            while(res.next()){
                Venda v = new Venda();
                v.setCodigonf(res.getInt("codigonf"));
                v.setDataVenda(res.getDate("data"));
                v.setNomeCliente(res.getString("nome_cliente"));
                v.setValor(res.getDouble("valor"));

                lista.add(v);
            }
        }
        catch(SQLException e)
        {
            System.out.println(e);
        }

        return lista;

    }
    public static List<Produto> carregarProdutosDaVenda(int codigoNf){
        String select = "select mercadoitemvenda.*, mercadoproduto.*\n" +
                        "from mercadoitemvenda\n" +
                        "inner join mercadoproduto on mercadoproduto.codprod = mercadoitemvenda.codprod\n" +
                        "where codigonf = ?";
        List<Produto> produtos = new ArrayList<>();
        
        try{
            Connection c = new Conexao("DBDS3_06","123456").getConexao();
            PreparedStatement st = c.prepareStatement(select);
            st.setInt(1,codigoNf);
            ResultSet res = st.executeQuery();
            while(res.next()){
                String tipo = res.getString("tipo");
                Produto p = null;
                if(tipo.equals("AlimentoNaoPerecivel")){
                    p = new AlimentoNaoPerecivel();
                }
                else if(tipo.equals("AlimentoPerecivel")){
                    p = new AlimentoPerecivel();
                }
                else if(tipo.equals("HigienePessoal")){
                    p = new HigienePessoal();
                }
                else{ //tipo == roupa
                    p = new Roupa();
                }
                
                p.setNome(res.getString("nome"));
                p.setDescricao(res.getString("descricao"));
                p.setPreco(res.getDouble("preco"));
                p.setEspecial(res.getString("especial"));
                p.setQuantidade(res.getInt("quantprod"));
                produtos.add(p);
            }
        } catch(SQLException e){
            System.out.println("Exception em carregarProdutosDaVenda");
            System.out.println(e);
        }
        return produtos;
    }


}
