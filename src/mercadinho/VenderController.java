/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercadinho;

import java.net.URL;
import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author luisa
 */
public class VenderController implements Initializable {

    @FXML
    private TableView<Produto> produtos;
    @FXML
    private TableColumn<?, ?> nomeProd;
    @FXML
    private TableColumn<?, ?> preco;
    @FXML
    private TableView<Produto> venda;   
    @FXML
    private TableColumn<?, ?> quantEstoque;
    @FXML
    private TableColumn<?, ?> valorTotal;
    @FXML
    private TextField nomeCliente;
    @FXML
    private DatePicker dataVenda;
    @FXML
    private TextField quant;
    @FXML
    private TableColumn<?, ?> nomeVenda;
    @FXML
    private Label valorTotalVenda;

    /**
     * Initializes the controller class.
     */
    private Venda v = new Venda();
    private List<Produto> lista = new ArrayList<>();
    private double somaValorVenda = 0;
    @FXML
    private Label informa;
    @FXML
    private Button add;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        produtos.setItems(FXCollections.observableArrayList(Produto.getAtualmenteVendidos())); 
        
        nomeProd.setCellValueFactory(new PropertyValueFactory("nome"));
        preco.setCellValueFactory(new PropertyValueFactory("preco"));
        quantEstoque.setCellValueFactory(new PropertyValueFactory("quantidade"));
        
        nomeVenda.setCellValueFactory(new PropertyValueFactory("nome"));
        valorTotal.setCellValueFactory(new PropertyValueFactory("valorTotal"));
        add.setDisable(true); 
        
    }    

    @FXML
    private void vender(ActionEvent event) throws DataException, ParseException {
        v.setProdutosVenda(lista);
        v.setNomeCliente(nomeCliente.getText());
               
        java.util.Date data = Date.valueOf(dataVenda.getValue());
        java.sql.Date dataSql = new java.sql.Date(data.getTime());
        v.setDataVenda(dataSql);
        
        v.insertVenda();        
        v.insertItemVenda();
        
        for(int i=0; i<v.getProdutosVenda().size(); i++){
            Produto p = v.getProdutosVenda().get(i);
            p.diminuirEstoque();
        }
        
        valorTotalVenda.setText("");
        quant.setText("");
        nomeCliente.setText("");
        dataVenda.setValue(null);
        
        informa.setText("Venda realizada.");  
        
        venda.getItems().clear();
    }

    @FXML
    private void voltar(ActionEvent event) {
        Mercadinho.trocaTela("Inicial.fxml");
    }

    @FXML
    private void add(ActionEvent event) {     
        Produto p = produtos.getSelectionModel().getSelectedItem();
        Produto copiado = p.copiar();
        
        int q = Integer.parseInt(quant.getText());
        System.out.println(p.getQuantidade());
        if(q<=p.getQuantidade() && q!=0){
            copiado.setQuantidade(Integer.parseInt(quant.getText()));        
            venda.getItems().add(copiado);

            lista.add(copiado);        
            somaValorVenda+= copiado.getValorTotal();

            v.setValor(somaValorVenda);

            valorTotalVenda.setText(v.getValor() + "");

            atualizarTabela(); //somente na tabela fxml, nao altera o banco de dados.
            quant.setText("");    
            produtos.getSelectionModel().clearSelection();
            //selecionado();
            add.setDisable(true); 
        }
        else{
            if(q==0){
                informa.setText("Nenhuma quantidade.");                
            }
            else{
                informa.setText("Estoque insuficiente.");
            }            
        }
        
    }
    
    private void atualizarTabela(){
        Produto p = produtos.getSelectionModel().getSelectedItem();
        p.setQuantidade(p.getQuantidade() - Integer.parseInt(quant.getText()));
        produtos.refresh();
    }    

    @FXML
    private void selecionado() {        
        add.setDisable(false);
        informa.setText("");
    }

    
}
