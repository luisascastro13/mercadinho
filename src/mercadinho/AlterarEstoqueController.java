/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercadinho;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author luisa
 */
public class AlterarEstoqueController implements Initializable {

    @FXML
    private TableView<Produto> resultado;
    @FXML
    private TableColumn<?, ?> nome;
    @FXML
    private TableColumn<?, ?> descricao;
    @FXML
    private TableColumn<?, ?> quantidade;
    @FXML
    private TextField novaQuantidade;
    @FXML
    private Label informa;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        resultado.setItems(FXCollections.observableArrayList(Produto.getAtualmenteVendidos())); 
        nome.setCellValueFactory(new PropertyValueFactory("nome"));
        descricao.setCellValueFactory(new PropertyValueFactory("descricao"));
        quantidade.setCellValueFactory(new PropertyValueFactory("quantidade"));
    }    

    @FXML
    private void pesquisar(ActionEvent event) {
        
    }

    @FXML
    private void alterar(ActionEvent event) {
        Produto p = resultado.getSelectionModel().getSelectedItem();
        try{
            int quant = Integer.parseInt(novaQuantidade.getText());
            if(quant>=0){
                p.setQuantidade(Integer.parseInt(novaQuantidade.getText()));
                p.atualizarEstoque();
                resultado.refresh();
                informa.setText("Quantidade alterada.");
            }
            else{
                informa.setText("Inválido. Digite um valor numérico positivo.");
            }
        }catch(NumberFormatException e){
            informa.setText("Inválido. Digite um valor numérico.");
        }
    }

    @FXML
    private void voltar(ActionEvent event) {
        Mercadinho.trocaTela("Inicial.fxml");
    }
    
}
